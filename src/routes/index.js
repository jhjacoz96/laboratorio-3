var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
const { Mensaje } = require("../models");

const autentificacion =require("../controllers/autentificacion"); 
const home = require("../controllers/home");
const usuario = require("../controllers/usuario");
const publicacion=require('../controllers/publicacion')
const seguir=require('../controllers/seguir')
const dinero=require('../controllers/dinero')
module.exports = (app) => {
 
const ap ='/api';
router.route(ap+'/login').post(autentificacion.login);
router.route(ap+'/rol').get(usuario.generar);
router.route(ap+'/user').get( usuario.consultar).post(usuario.registrar);
router.route(ap+'/user-admin').post(usuario.registrarAdmin);
router.route(ap+'/user/:id').get(ensureToken, usuario.editar).put(ensureToken,usuario.actualizar).delete(usuario.eliminar); 
router.route(ap+'/user/imagenPerfil').post(ensureToken,usuario.imagenPerfil)
router.route(ap+'/buscar/perfil/:id').get(ensureToken, usuario.perfil)

//ggregar y eliminar tarjetas
router.route(ap+'/agregar-tarjeta').put(ensureToken,usuario.agregarTarjeta)
router.route(ap+'/eliminar-tarjeta/:id').delete(ensureToken,usuario.eliminarTarjeta)

//consultar todas las publicaciones de seguidores
router.route(ap+'/publicacion').get(ensureToken,publicacion.consultars)
router.route(ap+'/publicacion-ns').get(ensureToken,publicacion.consultarss)

//Consultar todas las publicaciones
router.route(ap+'/publicacion/all').get(ensureToken,publicacion.consultarAll)

router.get('/chat', async (req,res) => {
  let result = await Mensaje.find()
  res.send(result);
})

//rutas de publicaciones para el usuario
router.route(ap+'/user/publicacion').post(ensureToken,publicacion.crear).get(ensureToken,publicacion.consultar)
router.route(ap+'/user/publicacion/:id').delete(ensureToken, publicacion.eliminar).get(ensureToken, publicacion.editar).put(ensureToken, publicacion.actualizar)
router.route(ap+'/user/buscar/publicaciones/:id').get(ensureToken, publicacion.publicacionesUser)
router.route(ap+'/user/buscar/publicaciones').get(ensureToken, publicacion.buscarPublicacion)
router.route(ap+'/user/publicacion/comentario/:id').post(ensureToken, publicacion.comentario)
router.route(ap+'/user/publicacion/comentario/:idP/:idC').get(ensureToken,publicacion.eliminarComentario)
router.route(ap+'/user/publicacion/like/:id').put(ensureToken, publicacion.like)

//seguir perfil
router.route(ap+'/seguir/:id').get(ensureToken,seguir.seguir)

//Permisos 
router.route(ap+'/dinero').get(ensureToken,dinero.consultar_dinero_usuario).put(ensureToken,dinero.cobrar);
router.route(ap+'/dinero-all').get(ensureToken,dinero.consultaGenerica);
router.get("/", home.index);


  function ensureToken(req, res, nex) {
    
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader != "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
    console.log(jwt.decode(bearerToken));  
     jwt.verify(bearerToken,'my_secret_ke',(err,data) =>{
        if(err) {
            res.status(403).json({ mensaje: "Usuario no Autenticado"});
        }else{
            req.token = bearerToken; 
            nex();
        }
     });
    } else {
        res.status(403).json({ mensaje: "Usuario no Autenticado"});
    }
  }
-

  app.use(router);

  
};
