const express = require('express');
const config = require('./server/config');
const bodyParser = require("body-parser"); 
var socket = require("socket.io"); 
const { Mensaje } = require("./models");


require('./database');

const app =config(express());


const server =  app.listen(app.get('port'),()=>{
    console.log('Server on port',app.get('port'));
}) 

let io =  socket(server);
let user_socket = {};

io.on("connection", function(socket){
  console.log("Socket Connection Established with ID :"+ socket.id)
 
  socket.on("chat", async function(chat){
    console.log(chat);
   // chat.created = new Date()
 //   let response = await new message(chat).save()
       socket.broadcast.emit("chat",chat);
  })


socket.on("increment",(counter)=>{
	console.log("incrementando");
	console.log(counter);
	io.sockets.emit('COUNTER_INCREMENT',counter+1);
});

socket.on("decrement",(counter)=>{
	console.log("decrementando");
	io.sockets.emit('COUNTER_DECREMENT',counter-1);
});

socket.on('login',(username)=>{
	if(user_socket[username]){
		socket.emit('USER_EXISTIS');
		return;
	}
	console.log(username);

	socket.username = username;
	user_socket[username]= username;
	
	console.log(user_socket);
	socket.emit('LOGIN',{
		username: socket.username,
		user_socket
	});


	socket.broadcast.emit('USER_JOINED',{
		username: socket.username,
		user_socket
	});
});



	socket.on('newMessage',(message)=>{
		console.log("nuevo mensaje "+message);
		socket.broadcast.emit("NEW_MESSAGE", socket.username + ':' + message);
		socket.emit('NEW_MESSAGE','Yo: '+message);
	});



	socket.on('disconnect',()=>{
		if(user_socket[socket.username]){
			delete user_socket[socket.username];
			socket.broadcast.emit('USER_LEFT',{
				username : socket.username,
				user_socket
			});
		}
	})
})


app.get('/chat', async (req,res) => {
  let result = await Mensaje.find()
  res.send(result);
})
