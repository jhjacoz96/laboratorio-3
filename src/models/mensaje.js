const mongoose=require('mongoose')
const {Schema}=mongoose;
const MensajeSchema =new Schema({
    'handle' : String,
    'message' : String,
    'created' : Date
});

module.exports=mongoose.model('Mensaje',MensajeSchema);