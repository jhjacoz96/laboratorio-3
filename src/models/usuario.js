const mongoose=require('mongoose')
const {Schema}=mongoose;
const bcrypt=require('bcryptjs');

const UsuarioSchema =new Schema({
   
    correo:{type:String,required:true},
    password:{type:String,required:true},
    activo:{type:Boolean, default:true},
    rol_id:{type:Schema.Types.ObjectId,ref:'Rol'},
});

UsuarioSchema.methods.encryptPassword= async (password)=>{

    const salt = await bcrypt.genSalt(10);

    const hash = bcrypt.hash(password,salt);

    return hash;
  
};

UsuarioSchema.methods.matchPassword = async function (password,password2){

    return await bcrypt.compare(password,this.password);
    
};

module.exports=mongoose.model('Usuario',UsuarioSchema);