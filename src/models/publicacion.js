const mongoose=require('mongoose')
const {Schema}=mongoose;

const path= require('path')
var Perfil = mongoose.model('Perfil');

const PublicacionSchema =new Schema({
    titulo:{type:String,required:true},
    descripcion:{type:String,required:true},
    activo:{type:Boolean,default:true},
    fecha:{type:Date, default:Date.now},
    perfil_id:{type:Schema.Types.ObjectId,ref:'Perfil'},
    cantidadLike:{type:Number,default:0},
    like:[
        {type:Schema.Types.ObjectId,ref:'Perfil'}
    ],
    comentarios:[
        {
            fecha:{type:Date, default:Date.now},
            comentario:{type:String,required:true},
            perfil_id:{type:Schema.Types.ObjectId,ref:'Perfil'}
        }
    ],
    imagen:{type:String,required:true}
});




module.exports=mongoose.model('Publicacion',PublicacionSchema);