const mongoose=require('mongoose')
const {Schema}=mongoose;

const path= require('path')

const PerfilSchema =new Schema({
    nombre:{type:String,required:true},
    apellido:{type:String,required:true},
    sexo:{type:String,required:true},
    edad:{type:Number,required:true},
    nick:{type:String,required:true},
    acerca_de_ti:{type:String,required:true},
    nivel:{type:Number, default:true},  
    tarjetas:[
        {
            numero:{type:String},
            tipo:{type:String},
            fechaExpiracion:{type:Date},
            codigo:{type:String}
        }
    ],
    usuario_id:{
        type:String
    },
    imagen:{type:String},
    siguiendo: [{
        type: Schema.Types.ObjectId,
        ref: 'Perfil'
    }],
    seguidores: [{
        type: Schema.Types.ObjectId,
        ref: 'Perfil'
    }],
    activo:{type:Boolean, default:true}
});

PerfilSchema.virtual('uniqueId').get(function(){
    return this.imagen.replace(path.extname(this.filemane), '')
})

module.exports=mongoose.model('Perfil',PerfilSchema);