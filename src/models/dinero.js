const mongoose=require('mongoose')
const {Schema}=mongoose;

const path= require('path')

const DineroSchema =new Schema({

    perfil_id:{type:Schema.Types.ObjectId,ref:'Perfil'},    
    monto :{type:Number,required:true},
});


module.exports=mongoose.model('Dinero',DineroSchema);