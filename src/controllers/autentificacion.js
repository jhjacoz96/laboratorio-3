const { Usuario, Perfil } = require("../models");
const Rol = require("../models/rol");
const bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

const ctrl = {};

ctrl.login = async (req, res) => {
  const { correo, password } = req.body;

    
      if ( correo != undefined || password != undefined) {
        const user = await Usuario.findOne({ correo: correo, activo: true }).populate("rol_id");
        if (user === null) {
          return res.status(401).json({
            mensaje: "El usuario no se encuentra registrado en la aplicacion",
            ok:false
          });
        } else {
          const bool = await bcrypt.compare(password, user.password);
          if (bool) {
            const token = jwt.sign({ user }, "my_secret_ke");

            const perfil = await  Perfil.findOne({ usuario_id: user._id})
           
            
            return res.status(200).json({ 
              token: token, 
              rol:user.rol_id,
              perfil: perfil,
              ok:true,
              mensaje:'Autenticacíon exitosa'
            });
          } else {
            return res.status(401).json({
              mensaje: "La contraseña no coincide con la registrada",
              ok:false
            });
          }
        }

      }
      else {
       return res.status(401).json({
            mensaje: "El correo y la contraseña son campos requeridos ",
            ok:false
          });
      }

    

};

module.exports = ctrl;
