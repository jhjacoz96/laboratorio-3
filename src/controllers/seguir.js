const ctrl={}

const path= require('path')
const {randomNumber}=require('../helpers/libs')
const fs=require('fs-extra')
var jwt = require("jsonwebtoken");

const Usuario=require('../models/usuario');
const Perfil=require('../models/perfil');
const Publicacion=require('../models/publicacion');

ctrl.seguir=async (req,res)=>{
    const id=req.params.id

    try {
        
        const perfilSiguiendo= await Perfil.findOne({_id:id}).populate('Perfil')
    
        var count = perfilSiguiendo.seguidores.length
        if(count+1 >99*perfilSiguiendo.nivel && perfilSiguiendo.nivel<3){

        await perfilSiguiendo.updateOne({ nivel: perfilSiguiendo.nivel+1 }, 
                { new: true });
        await perfilSiguiendo.save();

        }

        const usuario= await jwt.decode(req.token)
        const perfilSeguidor = await Perfil.findOne({usuario_id:usuario.user._id})
    
        if(perfilSeguidor._id==id) {
            res.status(400).json({ok:false, mensaje: "No se puede seguir a si mismo"})

        }else{
           
            if(perfilSiguiendo.seguidores.includes(perfilSeguidor._id)){
               
                const arrayIndex1 = await perfilSeguidor.siguiendo.indexOf(perfilSiguiendo._id);
                await perfilSeguidor.siguiendo.splice(arrayIndex1,1);
                await perfilSeguidor.save()
    
                const arrayIndex = await perfilSiguiendo.seguidores.indexOf(perfilSeguidor._id);
                await perfilSiguiendo.seguidores.splice(arrayIndex,1);
                await perfilSiguiendo.save()
    
                if(perfilSiguiendo.seguidores.length -1 < perfilSiguiendo.nivel*100 &&  perfilSiguiendo.nivel>1){
                    await  perfilSiguiendo.updateOne({ nivel: perfilSiguiendo.nivel -1  },{new:true});
                    await perfilSiguiendo.save();
                }
             
                return res.status(200).json({
                    ok:true,
                    seguir:false,
                    mensaje: "Perfil dejado de seguir",
                    perfilSiguiendo:perfilSiguiendo
                  });
            }else{
    
                

                await perfilSiguiendo.seguidores.push(perfilSeguidor)
                await perfilSiguiendo.save()
    
                await perfilSeguidor.siguiendo.push(perfilSiguiendo)
                await perfilSeguidor.save() 
                 
                 return res.status(200).json({
                    ok:true,
                    seguir:true,
                    mensaje: "Perfil seguido",
                    perfilSiguiendo:perfilSiguiendo
                  });
    
            }
        }

    } catch (error) {
        
        return res.status(400).json({
            ok:false,
            mensaje: "Ocurrio un error ",
            error:error
          });

    }


}

module.exports =ctrl