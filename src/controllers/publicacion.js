const ctrl = {};

const path = require("path");
const { randomNumber } = require("../helpers/libs");
const fs = require("fs-extra");
var jwt = require("jsonwebtoken");
const { check, validationResult } = require('express-validator');
const { Dinero } = require('../models/index');
const Usuario = require("../models/usuario");
const Perfil = require("../models/perfil");
const Publicacion = require("../models/publicacion");
const Imagen = require("../models/imagen");


ctrl.consultarAll= async (req,res)=>{

  try {
    
    const publicaciones= await Publicacion.find()
    .populate("Perfil")
    .populate("perfil_id")
    .populate("comentarios")
    .sort({fecha:-1})

    return res.status(200).json({
      ok:true,
      publicaciones:publicaciones,
      mensaje: "Publicaciones consultadas con exito"
    });

  } catch (error) {
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });
  }



}


ctrl.publicacionesUser = async (req,res)=>{
  var id= req.params.id
  try {
    
    const perfil = await Perfil.findById(id)

    const publicaciones = await Publicacion.find({perfil_id:perfil._id}).populate('perfil_id')

    if(publicaciones.length>0){
      return res.status(200).json({
        mensaje:'Publicaiones consultadas con exito',
        publicaciones:publicaciones,
        ok:true
      });
    }else{
      return res.status(400).json({
        mensaje:'Este usuario no posee publicaciones',
        ok:false
      });
    }

} catch (error) {
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });
  }
}


ctrl.consultarss = async (req, res) => {
  const usuario = jwt.decode(req.token);
  const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
  
  try {
    
    const publicacion = await Publicacion.find().populate("perfil_id").sort({fecha:-1});
    const publi = [];
    for (let i = 0; i < publicacion.length; i++) {
      const seguidores = publicacion[i].perfil_id.seguidores;
      if(!seguidores.includes(perfil._id)){
        publi.push(publicacion[i]);
      }
    }

    return res.status(200).json({
      mensaje:'Publicaiones consultadas con exito',
      publicacion:publi,
      ok:true
    });


  } catch (error) {
    
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });

  }

    

};

ctrl.consultars = async (req, res) => {
  const usuario = jwt.decode(req.token);
  const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
  
  try {
    
    const publicacion = await Publicacion.find()
      .populate("Perfil")
      .populate({
        path: "perfil_id",
        populate: [{ path: "siguiendo" }, { path: "seguidores" }],
      })
      .sort({fecha:-1});
  
    const publi = [];
    for (let i = 0; i < publicacion.length; i++) {
      const seguidores = publicacion[i].perfil_id.seguidores;
      for (let j = 0; j < seguidores.length; j++) {
        if (seguidores[j]._id.toString() == perfil._id.toString()) {
          publi.push(publicacion[i]);
        }
      }
    }
  
    return res.status(200).json({
      mensaje:'Publicaiones consultadas con exito',
      publicacion:publi,
      ok:true
    });

  } catch (error) {
    
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });


  }



};

ctrl.buscarPublicacion = async (req,res) => {
  console.log('hol')
  try {
    
    const usuario = jwt.decode(req.token);
    console.log(usuario)
    const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
    console.log(perfil)
    const publicacion = await Publicacion.find({ perfil_id: perfil._id })
    .populate("Perfil")
    .populate("perfil_id")
    .populate("comentarios")
    .populate({
      path: "perfil_id",
      populate: [{ path: "siguiendo" }, { path: "seguidores" }],
    })
    .sort({fecha:-1})

    return res.status(200).json({
      ok:true,
      publicacions:publicacion,
      mensaje: "Publicaciones encontradas con éxito"
    });

  } catch (error) {

    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });
    
  }


}

ctrl.crear = async (req, res) => {


  await check('titulo',"Titulo requerido").notEmpty().run(req); 
  await check('descripcion',"Descripcion requerido").notEmpty().run(req); 
   

  const result = validationResult(req);

    
  if (!result.isEmpty()) {
      return res.status(422).json({ 
          errors: result.array(),
          ok:false,
          mensaje:'Ha ocurrido un error'
       });
  }else{

    try {
      
      const { titulo, descripcion } = req.body;
      const usuario = jwt.decode(req.token);
     
  
        if (req.file==null) {
          return res.status(401).json({
            mensaje: "Debe insertar una imagen",
            ok:false
          });
        } else {
          const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
      
          
          
          const imagenes = req.file;
         
            const guardarImagen = async () => {
    
              const imgUrl = randomNumber();
              const imagen = await Publicacion.find({ imagen: imgUrl });
              if (imagen.length > 0) {
                guardarImagen();
              } else {
                console.log(imagenes)
                const imageTempPath = imagenes['path'];
                const ext = path.extname(imagenes.originalname).toLowerCase();
                const targetPath = path.resolve(`src/public/upload/${imgUrl}${ext}`);
      
                if (
                  ext === ".png" ||
                  ext === ".jpg" ||
                  ext === ".jpeg" ||
                  ext === ".git"
                ) {
                  await fs.rename(imageTempPath, targetPath);
      
    
                  const newPublicacion = new Publicacion({
                    titulo,
                    descripcion,
                    perfil_id: perfil,
                    imagen:  imgUrl + ext 
                  }).populate("Perfil")
                  .populate("perfil_id")
              
                  await newPublicacion.save();
    
                  return res.status(200).json({
                    mensaje:'Publicaion creada con exito',
                    publicacion:newPublicacion,
                    ok:true
                    });
                
                } else {
                  await fs.unlink(imageTempPath);
                  return res.status(500).json({ok:false, mensaje: "error solo imagenes" });
                }
              }
            };
            guardarImagen ();
        
        }

    } catch (error) {
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });
  }

  }

};

ctrl.eliminar = async (req, res) => {
  const id = req.params.id;


    const publicacion = await Publicacion.findOneAndDelete({ _id: id });

    if (!publicacion) {
      return res.status(400).json({
        ok:false,
        mensaje: "Ocurrio un error ",
        error,
      });
    }

    return res.status(200).json({
      ok:true,
      publicacion:publicacion,
      mensaje: "Publicación eliminada con éxito"
    }); 
   
  
};

ctrl.editar = async (req, res) => {
  const _id = req.params.id;

  try {
    const publicacion = await Publicacion.findById(_id)
    .populate({
      path: "comentarios",
      populate: { path: "perfil_id" },
    });
   


    return res.status(200).json({
      ok:true,
      publicacion:publicacion,
      mensaje: "Publicación encontrada con éxito"
    });
  } catch (error) {
   
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error,
    });
  }
};

ctrl.actualizar = async (req, res) => {


  await check('titulo',"Titulo requerido").notEmpty().run(req); 
  await check('descripcion',"Descripcion requerido").notEmpty().run(req);  

  const result = validationResult(req);

    
  if (!result.isEmpty()) {
      return res.status(422).json({ 
          errors: result.array(),
          ok:false,
          mensaje:'Ha ocurrido un error'
       });
  }else{
    const _id = req.params.id;
    const { titulo, descripcion } = req.body;
  
    try {
      const publicacion = await Publicacion.findOneAndUpdate(
        { _id: _id },
        { titulo, descripcion },
        { new: true }
      );
      return res.status(200).json({
        ok:true,
        publicacion:publicacion,
        mensaje: "Publicación actualizada con éxito"
      });
    } catch (error) {
      return res.status(400).json({
        ok:false,
        mensaje: "Ocurrio un error ",
        error,
      });
    }
  }

};

ctrl.consultar = async (req, res) => {
  const usuario = jwt.decode(req.token);
  try {
    const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
    const publicacion = await Publicacion.find({ perfil_id: perfil.id })
      .populate("Perfil")
      .populate("perfil_id");

      return res.status(200).json({
        ok:true,
        publicacion:publicacion,
        mensaje: "Publicaciones encontradas con éxito"
      });
  } catch (error) {
    res.status(404).json({
      ok:false,
      mensaje: "Ocurrioun error",
      error,
    });
  }
};

ctrl.comentario = async (req, res) => {


  await check('comentario',"Comentario requerido").notEmpty().run(req);  

  const result = validationResult(req);

    
  if (!result.isEmpty()) {
      return res.status(422).json({ 
          errors: result.array(),
          ok:false,
          mensaje:'Debe ingresar un comentario'
       });
  }else{ 
    try {
      const { comentario } = req.body;
      const publicacion = await Publicacion.findOne({ _id: req.params.id })
        .populate("Perfil")
        .populate("perfil_id");
      const usuario = await jwt.decode(req.token);
      const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });

      publicacion.comentarios.push({
        comentario: comentario,
        perfil_id: perfil,
      });

      publicacion.save();

  

      res.status(200).json({
        ok:true,
        mensaje: "Comentario creado con éxito",
        publicacion:publicacion
      });

    } catch (error) {
      res.status(401).json({
        ok:false,
        mensaje: "Ah ocurrido un error",
        error,
      });
    }
  }
};

ctrl.eliminarComentario = async (req, res) => {
  const idP = req.params.idP;
  const idC = req.params.idC;

  try {
    const publicacion = await Publicacion.findOneAndUpdate(
      { _id: idP },
      { $pull: { comentarios: { _id: idC } } },
      { new: true }
    )
      .populate("Perfil")
      .populate("perfil_id")
      .populate("comentarios");
      
    if (!publicacion) {
      return res.status(400).json({
        ok:false,
        mensaje: "Ocurrio un error ",
        error,
      });
    }else{

      res.status(200).json({
        ok:true,
        mensaje: "Comentario eliminado con éxito",
        comentario:publicacion
      });
    }


  } catch (error) {
    return res.status(400).json({
      ok:false,
      mensaje: "Ocurrio un error ",
      error:error
    });
  }
};

ctrl.like = async (req, res) => {
  if (!req.params.id) {

    res.status(401).json({
      ok:false,
      mensaje: "Debe enviar el id de la publicación"
    });
   
  } else {
    try{

      const id = req.params.id;
      const usuario = jwt.decode(req.token);
      const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
      const publicacion = await Publicacion.findOne({ _id: id })
        .populate("Perfil")
        .populate("perfil_id");
      var monto =  0.001*perfil.nivel;
      var monto2 = 0;
      if (publicacion.like.includes(perfil._id)) {
        publicacion.cantidadLike--;
        const arrayIndex = await publicacion.like.indexOf(perfil._id);
        await publicacion.like.splice(arrayIndex, 1);
        await publicacion.save();
        const dinero = await Dinero.findOne({
          perfil_id: publicacion.perfil_id["_id"],
        });

        if(JSON.stringify(perfil._id)!=JSON.stringify(publicacion.perfil_id._id)){

          if(dinero.monto>0){
            var monto2 = dinero.monto - monto;
            const dinero2 = await Dinero.findOneAndUpdate(
              { _id: dinero._id },
              { monto: monto2 },
              { new: true }
            );       
          }

        }
      
     
          return res.status(200).json({
            ok:true,
            mensaje: "Dislike creado",
            publicacion:publicacion,
            like:false
          });
      
      } else {
          console.log(JSON.stringify(perfil._id))
          console.log(JSON.stringify(publicacion.perfil_id._id))
        if(JSON.stringify(perfil._id)!=JSON.stringify(publicacion.perfil_id._id)){

          const dinero = await Dinero.findOne({
            perfil_id: publicacion.perfil_id["_id"],
          });
          if (dinero != null) {
            var monto2 = dinero.monto + monto;
            const dinero2 = await Dinero.findOneAndUpdate(
              { _id: dinero._id },
              { monto: monto2 },
              { new: true }
            );
          } else {
            const newDinero = new Dinero({
              monto,
              perfil_id: publicacion.perfil_id["_id"],
            });
            await newDinero.save();
          }

        }
  
        publicacion.cantidadLike++;
        await publicacion.like.push(perfil);
        await publicacion.save();

      
          return res.status(200).json({
            ok:true,
            mensaje: "Like creado",
            publicacion:publicacion,
            like:true
          });

      }
    }catch (error) {
      return res.status(400).json({
        ok:false,
        mensaje: "Ocurrio un error ",
        error:error
      });
    }
  }
};

module.exports = ctrl;
