const ctrl = {}


const path = require('path')
const { randomNumber } = require('../helpers/libs')
const fs = require('fs-extra')
var jwt = require("jsonwebtoken");
const { check, validationResult } = require('express-validator');
const Usuario = require('../models/usuario');
const Perfil = require('../models/perfil');
const Rol = require('../models/rol');


ctrl.generar = async (req, res) => {
    try {
        const roles = await Rol.find()
        if (roles.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Ya ha registrado los roles'
            })
        } else {
            var nombre1 = 'cliente'
            var nombre2 = 'administrador'
            const newRolCliente = await new Rol({ nombre: nombre1 })
            await newRolCliente.save()
            const newRolAdministrador = await new Rol({ nombre: nombre2 })
            await newRolAdministrador.save()

            return res.status(200).json({
                ok: true,
                mensaje: 'Roles registrados'
            })
        }
    } catch (error) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Ocurrio un error',
            error: error
        })
    }


}

ctrl.registrar = async (req, res) => {


    await check('correo').isEmail().withMessage('Formato de correo incorrepto').notEmpty().withMessage('Correo requerido').run(req);
    await check('nombre', "Nombre requerido").notEmpty().run(req);
    await check('password', "Contraseña requerida").notEmpty().run(req);
    await check('apellido', "Apellido requerido").notEmpty().run(req);
    await check('sexo', "Sexo requerido").notEmpty().run(req);
    await check('nick', "Nombre de usuario requerido").notEmpty().run(req);
    await check('edad').notEmpty().withMessage('Edad requerida').isInt().withMessage('Formato de edad incorrepto').run(req);
    await check('acerca_de_ti', "Comentario requerido").notEmpty().run(req);

    const result = validationResult(req);

    try {
        if (!result.isEmpty()) {
            return res.status(422).json({
                errors: result.array(),
                ok: false,
                mensaje: 'Ha ocurrido un error'
            });
        } else {

            const { correo, password, acerca_de_ti, nombre, apellido, sexo, edad, nick } = req.body;

            const correoUsuario = await Usuario.findOne({ correo: correo })
            const nickPerfil = await Perfil.findOne({ nick: nick })

            if (correoUsuario) {
                return res.status(400).json({
                    mensaje: 'El correo ya existe',
                    ok: false
                })
            } else {

                if (nickPerfil) {
                    res.status(400).json({
                        mensaje: 'El nombre de usuario ya existe',
                        ok: false
                    })
                } else {
                    const roles = await Rol.find()
                    if (roles.length == 0) {
                        var nombre1 = 'cliente'
                        var nombre2 = 'administrador'
                        const newRolCliente = await new Rol({ nombre: nombre1 })
                        await newRolCliente.save()
                        const newRolAdministrador = await new Rol({ nombre: nombre2 })
                        await newRolAdministrador.save()
                    }

                    const rol = await Rol.findOne({ nombre: 'cliente' })

                    const newUsuario = new Usuario({ correo, password })
                    newUsuario.password = await newUsuario.encryptPassword(password)
                    newUsuario.rol_id = rol._id

                    await newUsuario.save()

                    var nivel = 1

                    const newPerfil = new Perfil({ nombre, apellido, nivel, sexo, edad, acerca_de_ti, nick })

                    newPerfil.usuario_id = newUsuario.id

                    await newPerfil.save()

                    const token = jwt.sign({ newUsuario }, "my_secret_ke");
                    res.status(200).json({
                        token: token,
                        perfil: newPerfil,
                        ok: true,
                        mensaje: 'Usuario cliente registrado'
                    });

                }

            }

        }
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Ocurrio un error',
            error: error
        })
    }







}

ctrl.registrarAdmin = async (req, res) => {


    await check('correo').isEmail().withMessage('Formato de correo incorrepto').notEmpty().withMessage('Correo requerido').run(req);
    await check('nombre', "Nombre requerido").notEmpty().run(req);
    await check('password', "Contraseña requerida").notEmpty().run(req);
    await check('apellido', "Apellido requerido").notEmpty().run(req);
    await check('sexo', "Sexo requerido").notEmpty().run(req);
    await check('nick', "Nombre de usuario requerido").notEmpty().run(req);
    await check('edad').notEmpty().withMessage('Edad requerida').isInt().withMessage('Formato de edad incorrepto').run(req);
    await check('acerca_de_ti', "Comentario requerido").notEmpty().run(req);

    const result = validationResult(req);

    try {
        if (!result.isEmpty()) {
            return res.status(422).json({
                errors: result.array(),
                ok: false,
                mensaje: 'Ha ocurrido un error'
            });
        } else {

            const { correo, password, acerca_de_ti, nombre, apellido, sexo, edad, nick } = req.body;

            const correoUsuario = await Usuario.findOne({ correo: correo })
            const nickPerfil = await Perfil.findOne({ nick: nick })

            if (correoUsuario) {
                return res.status(400).json({
                    mensaje: 'El correo ya existe',
                    ok: false
                })
            } else {

                if (nickPerfil) {
                    res.status(400).json({
                        mensaje: 'El nombre de usuario ya existe',
                        ok: false
                    })
                } else {
                    const rol = await Rol.findOne({ nombre: 'administrador' })
                    if (!rol) {
                        return res.status(400).json({
                            mensaje: 'El rol administrador no está disponible',
                            ok: false
                        })
                    } else {

                        const newUsuario = new Usuario({ correo, password })
                        newUsuario.password = await newUsuario.encryptPassword(password)
                        newUsuario.rol_id = rol._id

                        await newUsuario.save()


                        const newPerfil = new Perfil({ nombre, apellido, sexo, edad, acerca_de_ti, nick })

                        newPerfil.usuario_id = newUsuario.id

                        await newPerfil.save()

                        const token = jwt.sign({ newUsuario }, "my_secret_ke");
                        res.status(200).json({
                            token: token,
                            perfil: newPerfil,
                            ok: true,
                            mensaje: 'Usuario administrador registrado registrado'
                        });
                    }
                }

            }

        }
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Ocurrio un error',
            error: error
        })
    }







}

ctrl.consultar = async (req, res) => {
    try {

        const usuarios = await Perfil.find({ activo: true })
            .populate("siguiendo")
            .populate("seguidores")

        return res.status(200).json({
            usuarios: usuarios,
            ok: true
        })

    } catch (error) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Ocurrio un error',
            error: error
        })
    }
}

ctrl.editar = async (req, res) => {

    try {

        const perfil = await Perfil.findById(req.params.id)
        const usuario = await Usuario.findOne({ _id: perfil.usuario_id })

        if (perfil) {
            return res.status(200).json({
                perfil: perfil,
                usuario: usuario,
                mensaje: 'Perfil encontrado',
                ok: true
            })
        } else {
            return res.status(400).json({
                mensaje: 'Perfil no encontrado',
                ok: false
            })
        }


    } catch (error) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Ocurrio un error',
            error: error
        })
    }


}

ctrl.perfil = async (req, res) => {

    try {

        const perfil = await Perfil.findById(req.params.id).populate('seguidores').populate('siguiendo')
        const usuario = await Usuario.findOne({ _id: perfil.usuario_id })
        if (perfil) {
            return res.status(200).json({
                perfil: perfil,
                usuario: usuario,
                mensaje: 'Perfil encontrado',
                ok: true
            })
        } else {
            return res.status(400).json({
                mensaje: 'Perfil no encontrado',
                ok: false
            })
        }


    } catch (error) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Ocurrio un error',
            error: error
        })
    }


}

ctrl.actualizar = async (req, res) => {


    await check('correo').isEmail().withMessage('Formato de correo incorrepto').notEmpty().withMessage('Correo requerido').run(req);
    await check('nombre', "Nombre requerido").notEmpty().run(req);
    await check('apellido', "Apellido requerido").notEmpty().run(req);
    await check('sexo', "Sexo requerido").notEmpty().run(req);
    await check('nick', "Nombre de usuario requerido").notEmpty().run(req);
    await check('edad').notEmpty().withMessage('Edad requerida').isInt().withMessage('Formato de edad incorrepto').run(req);
    await check('acerca_de_ti', "Comentario requerido").notEmpty().run(req);

    const result = validationResult(req);


    if (!result.isEmpty()) {
        return res.status(422).json({
            errors: result.array(),
            ok: false,
            mensaje: 'Ha ocurrido un error'
        });
    } else {

        const _id = req.params.id
        const usuario = await Usuario.findById(_id)
        const perfill = await Perfil.findOne({ usuario_id: usuario._id })


        const { correo, acerca_de_ti, nombre, apellido, sexo, edad, nick } = req.body;


        const correoUsuario = await Usuario.findOne({ correo: correo })

        const nickPerfil = await Perfil.findOne({ nick: nick })

        if (correoUsuario) {
            if (correoUsuario.correo != usuario.correo) {
                var existeCorreo = true
            }
        }

        if (nickPerfil) {
            if (nickPerfil.nick != perfill.nick) {
                var existenick = true
            }
        }



        if (existeCorreo == true) {

            return res.status(400).json({
                mensaje: 'El correo ya existe',
                ok: false
            })
        } else {

            if (existenick == true) {
                res.status(400).json({
                    mensaje: 'El nombre del ya existe',
                    ok: false
                })
            } else {

                const perfil = await Perfil.findOneAndUpdate({ usuario_id: usuario.id }, { acerca_de_ti, nombre, apellido, sexo, edad, nick }, { new: true })

                await Usuario.findOneAndUpdate({ _id: usuario.id }, { correo: correo }, { new: true })


                res.status(200).json({
                    perfil: perfil,
                    ok: true,
                    mensaje: 'Perfil actualizado'
                });

            }

        }

    }


}

ctrl.eliminar = async (req, res) => {
    const _id = req.params.id

    try {
        const perfilUsuario = await Perfil.findOneAndUpdate({ usuario_id: _id }, { activo: false }, { new: true })
        const usuario = await Usuario.findByIdAndUpdate({ _id }, { activo: false });


        if (!usuario) {
            return res.status(400).json({
                mensaje: 'Ocurrio un error',
                error
            })
        } else {
            return res.status(200).json({
                perfil: perfilUsuario,
                mensaje: 'Usuario inhabilitado',
                ok: true
            });

        }
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }



}


ctrl.imagenPerfil = (req, res) => {


    try {

        if (req.file == null) {
            return res.status(401).json({
                mensaje: "Debe insertar una imagen",
                OK: false
            });
        } else {

            const guardarImagen = async () => {

                const imgUrl = randomNumber()
                const imagen = await Perfil.find({ imagen: imgUrl })
                if (imagen.length > 0) {
                    guardarImagen()
                } else {

                    const imageTempPath = req.file.path;
                    const ext = path.extname(req.file.originalname).toLowerCase()
                    const targetPath = path.resolve(`src/public/upload/${imgUrl}${ext}`)

                    if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.git') {

                        await fs.rename(imageTempPath, targetPath)

                        const usuario = await jwt.decode(req.token)


                        const perfil = await Perfil.findOneAndUpdate({ usuario_id: usuario.user._id }, { imagen: imgUrl + ext }, { new: true })

                        return res.status(200).json({
                            perfil: perfil,
                            ok: true,
                            mensaje: 'Imagen de perfil actualizada'
                        });

                    } else {
                        await fs.inlink(imageTempPath)
                        return res.status(500).json({
                            error: 'error solo imagenes',
                            ok: false
                        })
                    }

                }

            }
            guardarImagen()
        }
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }

}

ctrl.agregarTarjeta = async (req, res) => {

    await check('numero', "Número requerido").notEmpty().run(req);
    await check('codigo', "Código de tarjeta requerido").notEmpty().run(req);
    await check('fechaExpiracion', "Formato de fecha inválido").notEmpty().run(req);
    await check('tipo', "Tipo de tarjeta requerido").notEmpty().run(req);

    const result = validationResult(req);

    if (!result.isEmpty()) {
        return res.status(422).json({
            errors: result.array(),
            ok: false,
            mensaje: 'Ha ocurrido un error'
        });
    } else {

        try {

            const usuario = jwt.decode(req.token);
            const { codigo, numero, fechaExpiracion, tipo } = req.body;
            const perfil = await Perfil.findOne({ usuario_id: usuario.user._id })
                .populate("Perfil")
                .populate("perfil_id");

            perfil.tarjetas.push({
                codigo: codigo,
                numero: numero,
                fechaExpiracion: fechaExpiracion,
                tipo: tipo
            });

            perfil.save();

            return res.status(200).json({
                ok: true,
                perfil: perfil,
                mensaje: 'tarjeta agregada'
            })


        } catch (error) {

            return res.status(400).json({
                ok: false,
                mensaje: 'Ocurrio un error',
                error
            })
        }
    }

}


ctrl.eliminarTarjeta = async (req, res) => {
    const usuario = jwt.decode(req.token);
    const id = req.params.id

    const perfil = await Perfil.findOneAndUpdate(
        { usuario_id: usuario.user._id },
        { $pull: { tarjetas: { _id: id } } },
        { new: true }
    )


    return res.status(200).json({
        ok: true,
        perfil: perfil,
        mensaje: 'tarjeta eliminado'
    })



}


module.exports = ctrl