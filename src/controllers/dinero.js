const ctrl = {};

const { Dinero, Perfil } = require("../models/index");
var jwt = require("jsonwebtoken");

ctrl.consultar_dinero_usuario = async (req, res) => {
  try {

    const usuario = jwt.decode(req.token);
    const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
    const dinero = await Dinero.findOne({ perfil_id: perfil._id }).populate(
      "Perfil"
    );
    return res.status(200).json({
      ok: true,
      dinero: dinero,
      mensaje: "Montos disponibles",
    });
  } catch (error) {
    return res.status(400).json({
      ok: false,
      mensaje: "Ocurrio un error ",
      error,
    });
  }
};

//Cobrar dinero
ctrl.cobrar = async (req, res) => {
  try {
    const usuario = jwt.decode(req.token);
    const perfil = await Perfil.findOne({ usuario_id: usuario.user._id });
    const dinero = await Dinero.findOne({ perfil_id: perfil._id });
    if (dinero.monto <= 0) {
      return res.status(400).json({
        ok: false,
        dinero: dinero,
        cobrar: false,
        mensaje: "No tiene saldo disponible",
      });
    } else {
     
      await dinero.updateOne({ monto:0 });
      await dinero.save();

      const dineroresult = await Dinero.findOne({ perfil_id: perfil._id });
      return res.status(200).json({
        ok: true,
        dinero: dineroresult,
        cobrar: true,
        mensaje: "Dinero Cobrado con éxito",
      });
    }
  } catch (error) {
    return res.status(400).json({
      ok: false,
      mensaje: "Ocurrio un error ",
      error:error,
    });
  }
};
ctrl.consultaGenerica = async (req, res) => {
  try {
    const dinero = await Dinero.find().sort({monto:-1}).limit(10).populate("perfil_id");
    const dineroall = await Dinero.find().populate("perfil_id");
  
    return res.status(200).json({
      ok: true,
      diezprimero: dinero,
      cobrar: true,
      mensaje: "Montos Registrados",
    });
  } catch (error) {
    return res.status(400).json({
      ok: false,
      mensaje: "Ocurrio un error ",
      error,
    });
  }
};

module.exports = ctrl;