const path = require('path');
const morgan=require('morgan');
const express=require('express');
const multer = require('multer');
const cors = require('cors');

const routes=require('../routes/index')

module.exports = app => {

    //setting
    app.set('port',process.env.PORT||3004);


    

 
    //--------middleware------//
    app.use(morgan('dev'));
    app.use(cors());

  
      app.use(multer({dest: path.join(__dirname, '../public/upload/temp')}).single('image')); 
  
    
    app.use(express.urlencoded({
        extended:false,
        useUnifiedTopology: true
    }))
    app.use(express.json());
                

  
    //---------route---------//
    routes(app);
    
    // Middleware para Vue.js router modo history
    const history = require('connect-history-api-fallback');
    app.use(history());
    //static files
    app.use('/public',express.static(path.join(__dirname,'../public')));



    return app;
}